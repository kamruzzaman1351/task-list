// Create var from DOM
const taskForm = document.getElementById("task-form");
const taskInput = document.getElementById("task");
const filter = document.getElementById("filter-task");
const taskList = document.querySelector("ul.collection");
const clearTask = document.getElementById("clear-tasks");
const showMsgEle = document.getElementById("show-msg");
const noTask = document.getElementById("no-task");
const cardList = document.querySelector(".card-action");

// Listen for all event in the DOM
listenAllEvent();
getAllTasks();

// All Event Function
function listenAllEvent() {
    // DOM Content Load Event
    // document.addEventListener("DOMContentLoaded", getAllTasks);
    // Create Task Event
    taskForm.addEventListener('submit', creatTask); 
    // Clear All the tasks Event
    clearTask.addEventListener("click", clearAllTasks);
    // Remove Single task Event
    cardList.addEventListener("click", removeSingleTask); 
    // Filter Task Event
    filter.addEventListener("keyup", filterTask); 

}

// Create Task
function creatTask(e) {
    e.preventDefault();
    if(taskInput.value !== "") {
        showMsg(taskInput);
        taskStorage(taskInput);
        createTaskList(taskInput.value);
        taskInput.value = "";
        noTask.innerText = "";
        // clearTask.removeAttribute('disabled');
        clearTask.disabled;
    } else {
        showMsg(taskInput);
    }
}
//  Creating Task list
function createTaskList(input) {
    const li = document.createElement("li")
    li.className = "collection-item";
    const taskText = document.createTextNode(input);
    li.appendChild(taskText);
    const link = document.createElement("a");
    link.className = "delete-item secondary-content";
    link.innerHTML = `<i class="fa fa-remove"></i>`;
    li.appendChild(link);
    taskList.appendChild(li);    
}

// Show Success and Error Msg
function showMsg(input) {
    if(input.value === "") {
        showMsgEle.style.display = "block";
        showMsgEle.className = "red-text";
        showMsgEle.innerText = "Input field is empty";
        setTimeout(function () {
            showMsgEle.style.display = "none";
        }, 3000);
    } else {
        showMsgEle.style.display = "block";
        showMsgEle.className = "green-text";
        showMsgEle.innerText = "Task Added";
        setTimeout(function () {
            showMsgEle.style.display = "none";
        }, 3000);
    }
}

// Store task into Local Storage
function taskStorage(input) {
    const task = input.value;
    let tasks;
    if(localStorage.getItem("tasks") === null) {
        tasks = []
    } else {
        tasks = JSON.parse(localStorage.getItem("tasks"));

    }
    tasks.push(task);
    localStorage.setItem("tasks", JSON.stringify(tasks));
    // localStorage.setItem("tasks", `${JSON.stringify(task)}`);
}

// Get Task From Local Storage
function getAllTasks() {
    const allTasks = localStorage.getItem("tasks");
    const tasksArr = JSON.parse(allTasks);
    if(allTasks !== null) {
        tasksArr.forEach(task => {
            createTaskList(task)
        });
    } else {
        noTask.innerText = "No Task Yet!";
        clearTask.setAttribute('disabled', '');
    }
    
}


// Clear All Task form local storage
function clearAllTasks(e) {  
    const allTasks = localStorage.getItem("tasks");
    const tasksArr = JSON.parse(allTasks);    
    if(allTasks !== null) {
        if(confirm("Do You want to delete all tasks")) {
            // Remove from LocalStorage
            localStorage.clear();
            // Remove From UI
            // taskList.remove();
            // taskList.innerHTML = "";
            while(taskList.firstChild) {
                taskList.removeChild(taskList.firstChild);
            }
            getAllTasks();
            e.preventDefault();
        }
    }    
}

// Remove Single Task
function removeSingleTask(e) {
    if(e.target.parentElement.classList.contains("delete-item")) {
        // Delete form UI
        if(confirm("Do You Want to Remove this Task?")) {
            e.target.parentElement.parentElement.remove();
            // Remove From LS
            let task = e.target.parentElement.parentElement.textContent;
            removeSingleTaskLS(task);
        }
    }
}

// Filter Task
function filterTask(e) {
    filterValue = e.target.value.toLowerCase();
    // UI Version
    let lists = document.querySelectorAll(".collection-item");
    lists.forEach((li) => {
        let listText = li.firstChild.textContent.toLowerCase();
        if(listText.indexOf(filterValue) != -1) {
            li.style.display = "block";
        } else {
            li.style.display = "none";
        }
    });

    // Local Storage
    // const allTasks = localStorage.getItem("tasks");
    // const tasksArr = JSON.parse(allTasks);
    // let lists = document.querySelectorAll(".collection-item");
    // tasksArr.forEach((task) => {
    //     if(task.toLowerCase().indexOf(filterValue) != -1) {
            
    //     } else {
           
    //     }
    // })
    
}

// Remove single task from Local Storage
function removeSingleTaskLS(taskContent) {
    const allTasks = localStorage.getItem("tasks");
    const tasksArr = JSON.parse(allTasks); 
    tasksArr.forEach((task,i,tasksArr) => {
        if(task === taskContent) {
            tasksArr.splice(i, 1);
        }
        
    })
    // if(tasksArr.length == 1) {
    //     clearTask.setAttribute('disabled', '');
    // }
    
    localStorage.setItem("tasks", JSON.stringify(tasksArr));
    // console.log(task);
}